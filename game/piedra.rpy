#este es un minijuego de piedra papel o tijera,
#completamente aleatorio y funcional :D
#Speedrunners, os gané :v

label piedresita:

  $ Enemigotiera = random.choice(Tirada)
  $ Puntos_propios = 0
  $ Puntos_enemigo = 0

  menu:
      "Piedra":
              jump piedra
      "Papel":
               jump papel
      "Tijera":
              jump tijera


label piedra:

     $ Tirada_propia = "Piedra"

     if Enemigotiera == "Piedra":
        "El enemigo tiene piedra"
        "Empate"
        jump piedresita

     if Enemigotiera == "Papel":
        "El enemigo tiene papel"
        "Perdiste... lo siento wacho :V"
        $ Puntos_enemigo = Puntos_enemigo +1
        jump piedresita

     if Enemigotiera == "Tijera":
        "El enemigo tiene tijera"
        $ Puntos_propios = Puntos_propios +1
        "Ganaste >:V"
        jump ganaste



label papel:

     $ Tirada_propia = "Papel"

     if Enemigotiera == "Piedra":
        "El enemigo tiene piedra"
        "Ganaste >:V"
        jump ganaste

     if Enemigotiera == "Papel":
        "El enemigo tiene papel"
        "Empate"
        jump piedresita

     if Enemigotiera == "Tijera":
        "El enemigo tiene tijera"
        "Perdiste... lo siento wacho :V"
        jump perdistewacho



label tijera:

     $ Tirada_propia = "Tijera"

     if Enemigotiera == "Piedra":
        "Tu enemigo ha sacado piedra"
        "Perdiste... lo siento wacho :V"
        jump perdistewacho

     if Enemigotiera == "Papel":
        "Tu enemigo ha sacado papel"
        "Ganaste >:V"
        jump ganaste

     if Enemigotiera == "Tijera":
        "El enemigo ha sacado tijera"
        "Empate"
        jump piedresita



label perdistewacho:

    n "Mira... perdiste contra un mono..."

    n "Debería darte vergüenza >:C"

    n "Weno te han metido un platano en la boca y no podrás hablar en 2 días XDXDXDXD"

    return


label ganaste:

    n "Tu aventura sigue :V no te han metido un platano en la boca u-u"
    jump Zeldaa
