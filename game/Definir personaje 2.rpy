
label tirada_inicial:

    $ Dado = random.randint(1,6)*7

    n "Tienes [Dado] puntos. Se repartirán entre las otras variables."

    "¿Cuántos puntos le quieres añadir a la fuerza? (se le sumarán otros 6)"

    $ Fuerza = renpy.input("Escribe el número de puntos u-u")

    $ Fuerza = int(Fuerza.strip())

    $ Dado = Dado -Fuerza
    #Acá está la segunda forma de definir las variables de tu personaje :]
    #Trata de recibir puntos y repartirlos entre las variables para quedarte con el valor
    #Que quieras :]

    $ Fuerza = Fuerza +6

    if Dado <= -1:
       "NO ME HAGAS TRAMPAS >:C YA NO LE PUEDES AÑADIR PUNTOS A ESTA HABILIDAD. TAMBIÉN TE DEJARÉ CON 0 PUNTOS >:u"
       $ Dado = 0
       $ Fuerza = 6

    "Tienes un [Fuerza] en fuerza"

    "Te quedan [Dado] puntos"

    if Dado <= 0:
       "Tienes 0 puntos :3 ya no puedes repartir mas! :D mejor para mi u-u"
       jump utilizalasuerte



    "Cuantos puntos le quieres añadir a la agilidad? (se le sumarán otros 12)"

    $ Agilidad = renpy.input("Escribe el numero de puntos u-u")

    $ Agilidad = int(Agilidad.strip())

    $ Dado = Dado -Agilidad

    $ Agilidad = Agilidad +12

    if Dado <= -1:
       "NO ME HAGAS TRAMPAS >:C YA NO LE PUEDES AÑADIR PUNTOS A ESTA HABILIDAD. TAMBIÉN TE DEJARÉ CON 0 PUNTOS >:u"
       $ Dado = 0
       $ Agilidad = 6

    "Tienes un [Agilidad] en agilidad"

    "Te quedan [Dado] puntos"

    if Dado <= 0:
       "Tienes 0 puntos :3 ya no puedes repartir mas! :D mejor para mi u-u"
       jump utilizalasuerte



    "Cuantos puntos le quieres añadir a la inteligencia? (se le sumarán otros 6)"

    $ Inteligencia = renpy.input("Escribe el numero de puntos u-u")

    $ Inteligencia = int(Inteligencia.strip())

    $ Dado = Dado -Inteligencia

    $ Inteligencia = Inteligencia +6


    if Dado <= -1:
       "NO ME HAGAS TRAMPAS >:C YA NO LE PUEDES AÑADIR PUNTOS A ESTA HABILIDAD. TAMBIÉN TE DEJARÉ CON 0 PUNTOS >:u"
       $ Dado = 0
       $ Inteligencia = 6

    "Tienes un [Inteligencia] en inteligencia"

    "Te quedan [Dado] puntos"

    if Dado <= 0:
       "Tienes 0 puntos :3 ya no puedes repartir mas! :D mejor para mi u-u"
       jump utilizalasuerte




    "Cuantos puntos le quieres añadir a la resistencia? (se le sumarán otros 12)"

    $ Resistencia = renpy.input("Escribe el numero de puntos u-u")

    $ Resistencia = int(Resistencia.strip())

    $ Dado = Dado -Resistencia

    $ Resistencia = Resistencia +12


    if Dado <= -1:
       "NO ME HAGAS TRAMPAS >:C YA NO LE PUEDES AÑADIR PUNTOS A ESTA HABILIDAD. TAMBIÉN TE DEJARÉ CON 0 PUNTOS >:u"
       $ Dado = 0
       $ Resistencia = 6

    "Tienes un [Resistencia] en resistencia"

    "Te quedan [Dado] puntos"

    if Dado <= 0:
       "Tienes 0 puntos :3 ya no puedes repartir mas! :D mejor para mi u-u"
       jump utilizalasuerte



    "Cuantos puntos le quieres añadir a la suerte? (se le sumarán otros 6)"

    $ Suerte = renpy.input("Escribe el numero de puntos u-u")

    $ Suerte = int(Suerte.strip())

    $ Dado = Dado -Suerte

    $ Suerte = Suerte +6


    if Dado <= -1:
       "NO ME HAGAS TRAMPAS >:C YA NO LE PUEDES AÑADIR PUNTOS A ESTA HABILIDAD. TAMBIÉN TE DEJARÉ CON 0 PUNTOS >:u"
       $ Dado = 0
       $ Suerte = 6

    "Tienes un [Suerte] en suerte"

    "Te quedan [Dado] puntos"

    if Dado <= 0:
       "Tienes 0 puntos :3 ya no puedes repartir mas! :D mejor para mi u-u"
       jump utilizalasuerte




    "Cuantos puntos le quieres añadir a la valentía? (se le sumarán otros 12)"

    $ Valentia = renpy.input("Escribe el numero de puntos u-u")

    $ Valentia = int(Valentia.strip())

    $ Dado = Dado -Valentia

    $ Valentia = Valentia +6


    if Dado <= -1:
       "NO ME HAGAS TRAMPAS >:C YA NO LE PUEDES AÑADIR PUNTOS A ESTA HABILIDAD. TAMBIÉN TE DEJARÉ CON 0 PUNTOS >:u"
       $ Dado = 0
       $ Valentia = 6

    "Tienes un [Valentia] en valentia"

    "Te quedan [Dado] puntos"

    if Dado <= 0:
       "Tienes 0 puntos :3 ya no puedes repartir mas! :D mejor para mi u-u"
       jump utilizalasuerte
