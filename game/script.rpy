﻿# Coloca el código de tu juego en este archivo.

# Declara los personajes usados en el juego como en el ejemplo:

define n = Character("Narrador")

# En efecto solo hay un "personaje"... de momento ;D
# acá se definen las variables 😊🔫 y se importan las bibliotecas ༼ つ ◕_◕ ༽つ

init:

    $ import random

    $ Dado = 0
    $ Fuerza = 0
    $ Agilidad = 0
    $ Inteligencia = 0
    $ Destreza = 0
    $ Resistencia = 0
    $ Suerte = 0
    $ Valentia = 0
    $ Sexo = ""
    $ RAenemigo = 10
    $ FA_enemigo = 0
    $ Contador = 0
    $ Tirada = ["Piedra","Papel","Tijera"]
    $ Tirada_propia = ""
    $ Tirada2 = ""
    $ Tirada2ene = ""
    $ Tirada2n = 0
    $ Tirada2enen = 0

# El juego comienza aquí.
# Esto yo no lo pude fur Ren'Py :[

label start:

#En este fichero solo está puesta la historia
#Después de hacer una cosa de otro fichero, toca un trocito de pastel 😊🔫A

  n "Apareces en un spawn de un juego de rol con un cofre"

  n "Al abrir el cofre te encuentras un libro"

  n "Es un libro de misiones que tienes que completar para salir"

  n "La primera es encontrar una botella (mágica por supuesto, si no no tendría sentido)"

  n "Pone que está en la casa de una vieja o algo"

  n "Por cierto tienes un equipo de amigos con poderes y eres un algo (sin spoilers por favor, ya te enterarás luego) >:D"

  n "Se llaman Leno, Crótalo y Luna. (En efeco apenas van a haber cameos :v)"

  n "Vamossss"

  n "Espera definamos tu personaje que se me olvida D:"

  jump definirnombre

label maspastel:

  n "Ok, llegaste a la casa de la vieja"

  n "Luna y Crótalo se meten en el baño a buscar"

  n "Tú te acercas a un armario en la cocina"

  n "Lo abres: no hay nada de interés, pero tiras algo al suelo. Lo colocas pero se vuelve a caer asi que lo dejas y te vas"

  n "Te metes detrás de la puerta, donde hay otra puerta más que da al mismo lugar que la puerta de la que está detrás"

  n "En una pequeña estantería encuentras la botella que está llena de líquido color ámbar"

  n "Aparece la vieja y entra en la cocina, donde ve a Leno, y se enzarzan en una pelea"

  n "Te escabulles por la segunda puerta, y encuentras a Luna y Crótalo"

  n "Salís por la puerta trasera, dejando solo a Leno"

  n "Al llegar al patio trasero, encontráis a 3 perros guardianes"

  n "Creas una tubería (Sí, puedes hacer eso, y no solo con tuberías, puedes crear todo lo que quieras UwU) y le das un coscorrón a 2 perros"

  n "El tercero huye"

  n "Entonces te das cuenta de que la vieja no es una vieja, es una bruja, y decides intervenir en el combate de Leno"

  jump combate1

label laa:

  n "Completaste tu primera misión"

  n "Más tarde harás más cosas con la botella, pero por ahora, se las das a Crótalo para que la guarde"

  n "En la casa encontráis una televisión muy desfasada, con una serie aburrida en pantalla"

  n "Encuentras unos CD's"

  n "Mientras intentas poner la liga de la justicia Leno se va a por palomitas"

  n "Entonces se abre un portal y os teletransporta a la serie"

  n "Aparecéis en una jungla y os dais cuenta de que ha aparecido una nueva misión"

  n "Tienes que domar al animal más fuerte de ese universo"

  n "La jungla esta llena de monos"

  n "Quieres jugar con ellos a piedra papel o tijera u-u"

  jump piedresita

label Zeldaa:

  n "Saliste de la serie :v"

  n "La misión era broma we"

  n "Tu verdadera misión es-"

  "Entras épicamente en un juego de Zelda"

  n "COMO LO ADIVINASTE???????? TU MISION ERA PASARTE ESTE CURSED ZELDA"

  n "Weno buena suerte... no les pegues a los pollos ;v"
