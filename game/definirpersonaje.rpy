label definirnombre:
    #Aca se define el nombre u3u
    $ Nombre = renpy.input("Escribe como se llamará tu personaje")
    $ Nombre = Nombre.strip()
    
    "El nombre de tu personaje es [Nombre]"
    
    menu:
          "Elige como quieres definir las variables de tu personaje"
          "Aleatorio":
              jump definirpersonaje
          "Repartiendo puntos a las variables":
              jump tirada_inicial
        
label definirpersonaje:
    #Y aca, empieza la primera forma de definir las variables
    #Trata de que el juego elija un valor aleatorio, y tu debes decidir si quieres que se elija
    #otro numero o quedarte con ese
    
    menu:
          "Tienes que elegir el sexo de tu personaje"
          "Mi personaje va a ser una chica":
               $ Sexo = "Chica"
               jump definirpersonajefuerza
          "Mi personaje va a ser un chico":
              $ Sexo = "Chico"
              jump definirpersonajefuerza
                  
                  
              
label definirpersonajefuerza:
    "Vamos a definir la fuerza"
    $ Fuerza = random.randint(0,6)+6
    "El valor de fuerza que has sacado es [Fuerza]"
    menu:
        "¿Estás de acuerdo con el valor de fuerza que has sacado?"
        "Si":
            jump definirpersonajeagilidad
        "No":
            $ Contador = Contador + 1
            if Contador >= 3:
               "Llegaste al límite de intentos"
               "Tu fuerza se queda en [Fuerza]"
               $ Contador = 0
               jump definirpersonajeagilidad
            jump definirpersonajefuerza
            
    
label definirpersonajeagilidad:
    "Vamos a definir la agilidad"
    $ Agilidad = random.randint(0,6)+12
    "El valor de agilidad que has sacado es [Agilidad]"
    menu:
         "Estás de acuerdo con el valor de agilidad que has sacado?"
         "Si":
             jump definirpersonajeinteligencia
         "No":
             $ Contador = Contador + 1
             if Contador >= 3:
               "Llegaste al límite de intentos"
               "Tu agilidad se queda en [Agilidad]"
               $ Contador = 0
               jump definirpersonajeinteligencia
             jump definirpersonajeagilidad
             
label definirpersonajeinteligencia:
    "Vamos a definir la inteligencia"
    $ Inteligencia = random.randint(0,6)+6
    "El valor de inteligencia que has sacado es [Inteligencia]"
    menu:
        "¿Estás de acuerdo con el valor de inteligencia que has sacado?"
        "Si":
            jump definirpersonajeresistencia
        "No":
            $ Contador = Contador + 1
            if Contador >= 3:
               "Llegaste al límite de intentos"
               "Tu inteligencia se queda en [Inteligencia]"
               $ Contador = 0
               jump definirpersonajeresistencia
            jump definirpersonajeinteligencia

label definirpersonajeresistencia:
    "Vamos a definir la resistencia"
    $ Resistencia = random.randint(0,6)+12
    "El valor de resistencia que has sacado es [Resistencia]"
    menu:
        "¿Estás de acuerdo con el valor de resistencia que has sacado?"
        "Si":
            jump definirpersonajesuerte
        "No":
            $ Contador = Contador + 1
            if Contador >= 3:
               "Llegaste al límite de intentos"
               "Tu resistencia se queda en [Resistencia]"
               $ Contador = 0
               jump definirpersonajesuerte
            jump definirpersonajeresistencia

        
label definirpersonajesuerte:
    "Vamos a definir la suerte"
    $ Suerte = random.randint(0,6)+6
    "El valor de suerte que has sacado es [Suerte]"
    menu:
        "¿Estás de acuerdo con el valor de suerte que has sacado?"
        "Si":
            jump definirpersonajevalentia
        "No":
            jump definirpersonajesuerte
            $ Contador = Contador + 1
            if Contador >= 3:
               "Llegaste al límite de intentos"
               "Tu suerte se queda en [Suerte]"
               $ Contador = 0
               jump definirpersonajevalentia
            jump definirpersonajesuerte
            
        
label definirpersonajevalentia:
    "Vamos a definir la valentía"
    $ Valentia = random.randint(0,6)+12
    "El valor de valentía que has sacado es [Valentia]"
    menu:
        "¿Estás de acuerdo con el valor de valentía que has sacado?"
        "Si":
            jump maspastel
        "No":
            $ Contador = Contador + 1
            if Contador >= 3:
               "Llegaste al límite de intentos"
               "Tu valentía se queda en [Valentia]"
               $ Contador = 0
               jump maspastel 
            jump definirpersonajevalentia
            
            
